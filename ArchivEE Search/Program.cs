﻿using ArchivEE;
using EELVL;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Linq.Expressions;

using static EELVL.Blocks;
using static Helpers;

Console.WriteLine();
WriteColor(ConsoleColor.Red, $@"     _             _     _      ");
WriteColor(ConsoleColor.Yellow, $@" _____ _____ {Environment.NewLine}");
WriteColor(ConsoleColor.Red, $@"    / \   _ __ ___| |__ (_)_   _");
WriteColor(ConsoleColor.Yellow, $@"| ____| ____|{Environment.NewLine}");
WriteColor(ConsoleColor.Red, $@"   / _ \ | '__/ __| '_ \| \ \ / /");
WriteColor(ConsoleColor.Yellow, $@"  _| |  _|  {Environment.NewLine}");
WriteColor(ConsoleColor.Red, $@"  / ___ \| | | (__| | | | |\ V /");
WriteColor(ConsoleColor.Yellow, $@"| |___| |___ {Environment.NewLine}");
WriteColor(ConsoleColor.Red, $@" /_/   \_\_|  \___|_| |_|_| \_/ ");
WriteColor(ConsoleColor.Yellow, $@"|_____|_____|{Environment.NewLine}");
Console.WriteLine();

#if DEBUG
string dbFile = PromptDefault("Input path to database file", "ArchivEE.sqlite3");
Console.WriteLine();
Console.WriteLine("Loading database...");
using ArchivEEDB db = new(dbFile, true);
#else
Console.WriteLine("Loading database...");
using ArchivEEDB db = new("ArchivEE.sqlite3");
#endif

Console.WriteLine($"{db.World.Count()} worlds found!");

Console.WriteLine();

List<(string name, IQueryable query)> parts = new();

IQueryable? selected = null;

while (true)
{
	List<(string msg, Func<(string name, IQueryable query)?> action)> options = new();

	options.AddRange(new (string msg, Func<(string name, IQueryable query)?> action)[] {
		("Start new query", () => FuncMenu("Query type", new (string msg, Func<(string name, IQueryable query)> action)[] {
			("Players", () => FuncMenu("Query", new (string msg, Func<(string name, IQueryable query)> action)[] {
				("All", () => ("All players", db.Player)),
				("By ID", () => {
					string[] ids = PromptList("Player IDs");
					return ("Players by ID", db.Player.Where(p => ids.Select(id => id.ToLower()).Contains(p.Id)));
				}),
				("By name", () => {
					string[] names = PromptList("Player names");
					return ("Players by name", db.Player.Where(p => names.Select(n => n.ToLower()).Contains(p.Name)));
				}),
				("Custom SQL", () => {
					string sql = PromptMultiline("SQL");
					return ("Players by custom SQL", db.Player.FromSqlRaw(sql));
				}),
			})),
			("Staff roles", () => FuncMenu("Query", new (string msg, Func<(string name, IQueryable query)> action)[] {
				("All", () => ("All staff", db.Staff)),
				("Custom SQL", () => {
					string sql = PromptMultiline("SQL");
					return ("Staff roles by custom SQL", db.Staff.FromSqlRaw(sql));
				}),
			})),
			("Crews", () => FuncMenu("Query", new (string msg, Func<(string name, IQueryable query)> action)[] {
				("All", () => ("All crews", db.Crew)),
				("By ID", () => {
					string[] ids = PromptList("Crew IDs");
					return ("Crews by ID", db.Crew.Where(c => ids.Select(id => id.ToLower()).Contains(c.Id)));
				}),
				("By name", () => {
					string[] names = PromptList("Crew names");
					return ("Crews by name", db.Crew.Where(c => names.Select(n => n.ToLower()).Contains(c.Name.ToLower())));
				}),
				("Custom SQL", () => {
					string sql = PromptMultiline("SQL");
					return ("Crews by custom SQL", db.Crew.FromSqlRaw(sql));
				}),
			})),
			("Worlds", () => FuncMenu("Query", new (string msg, Func<(string name, IQueryable query)> action)[] {
				("All", () => ("All worlds", db.World)),
				("By ID", () => {
					string[] ids = PromptList("World IDs");
					return ("Worlds by ID", db.World.Where(w => ids.Contains(w.Id)));
				}),
				("By name", () => {
					string[] names = PromptList("World names");
					return ("Worlds by name", db.World.Where(w => names.Select(n => n.ToLower()).Contains(w.Name.ToLower())));
				}),
				("Custom SQL", () => {
					string sql = PromptMultiline("SQL");
					return ("Worlds by custom SQL", db.World.FromSqlRaw(sql));
				}),
			})),
			("Campaigns", () => FuncMenu("Query", new (string msg, Func<(string name, IQueryable query)> action)[] {
				("All", () => ("All campaigns", db.Campaign)),
				("By ID", () => {
					string[] ids = PromptList("Campaign IDs");
					return ("Campaigns by ID", db.Campaign.Where(c => ids.Select(id => id.ToUpper()).Contains(c.Id)));
				}),
				("By name", () => {
					string[] names = PromptList("Campaign names");
					return ("Campaigns by name", db.Campaign.Where(c => names.Select(n => n.ToLower()).Contains(c.Name.ToLower())));
				}),
				("Custom SQL", () => {
					string sql = PromptMultiline("SQL");
					return ("Campaigns by custom SQL", db.Campaign.FromSqlRaw(sql));
				}),
			})),
			("Campaign worlds", () => FuncMenu("Query", new (string msg, Func<(string name, IQueryable query)> action)[] {
				("All", () => ("All campaign worlds", db.CampaignWorld)),
				("Custom SQL", () => {
					string sql = PromptMultiline("SQL");
					return ("Campaign worlds by custom SQL", db.CampaignWorld.FromSqlRaw(sql));
				}),
			})),
			("Completed campaign worlds", () => FuncMenu("Query", new (string msg, Func<(string name, IQueryable query)> action)[] {
				("All", () => ("All completed campaign worlds", db.CompletedCampaignWorld)),
				("Custom SQL", () => {
					string sql = PromptMultiline("SQL");
					return ("Completed campaign worlds by custom SQL", db.CompletedCampaignWorld.FromSqlRaw(sql));
				}),
			})),
		})),
	});

	if (selected != null) options.AddRange(new (string msg, Func<(string name, IQueryable query)?> action)[] {
		("Filter", () => FuncMenu("Filter by", selected switch {
			IQueryable<Player> players => new (string msg, Func<(string name, IQueryable query)> action)[] {
				("Energy", () => {
					int? min = PromptIntMaybe("Min");
					int? max = PromptIntMaybe("Max");
					Console.WriteLine();
					return ("Players by energy", players.Where(p => (min == null || p.Energy >= min) && (max == null || p.Energy <= max)));
				}),
				("Created", () => {
					DateTime? start = PromptDateMaybe("Created after");
					DateTime? end = PromptDateMaybe("Created before");
					Console.WriteLine();
					return ("Players by date created", players.Where(p => (start == null || p.Created >= start) && (end == null || p.Created <= end)));
				}),
				("Last logged in", () => {
					DateTime? start = PromptDateMaybe("Last logged in after");
					DateTime? end = PromptDateMaybe("Hasn't logged in since");
					Console.WriteLine();
					return ("Players by last login", players.Where(p => (start == null || p.LastLogin >= start) && (end == null || p.LastLogin <= end)));
				}),
			},
			IQueryable<Staff> staff => new (string msg, Func<(string name, IQueryable query)> action)[] {
				("Domain", () => {
					string domain = SelectMenu("Domain", new (string msg, string domain)[] {
						("EE", "EE"),
						("EEU", "EEU"),
						("Forums", "Forums"),
						("Discord", "Discord"),
					});
					return ("Staff by domain", staff.Where(s => s.Domain == domain));
				}),
				("Role", () => {
					(string domain, string role) = SelectMenu("Domain", new[] {
						("EE Owner", ("EE", "Owner")),
						("EE Developer", ("EE", "Developer")),
						("EE Graphics Designer", ("EE", "Graphics Designer")),
						("EE Moderator", ("EE", "Moderator")),
						("EE World Curator", ("EE", "World Curator")),
						("EE Guardian", ("EE", "Guardian")),
						("EEU Owner", ("EEU", "Owner")),
						("EEU Developer", ("EEU", "Developer")),
						("EEU Graphics Designer", ("EEU", "Graphics Designer")),
						("EEU Composer", ("EEU", "Composer")),
						("EEU Moderator", ("EEU", "Moderator")),
						("EEU World Curator", ("EEU", "World Curator")),
						("EEU Community Manager", ("EEU", "Community Manager")),
						("EEU Wiki Admin", ("EEU", "Wiki Admin")),
						("Forums Admin", ("Forums", "Admin")),
						("Forums Admin", ("Forums", "Moderator")),
						("Discord Owner", ("Discord", "Owner")),
						("Discord Moderator", ("Discord", "Moderator")),
						("Discord Consultant", ("Discord", "Consultant")),
						("Discord Event Manager", ("Discord", "Event Manager")),
					});
					return ("Staff by role", staff.Where(s => s.Domain == domain && s.Role == role));
				}),
				("Was staff on a specific date", () => {
					DateTime date = PromptDate("Date");
					Console.WriteLine();
					return ("Staff by date", staff.Where(s => s.Start <= date && (s.End == null || s.End >= date)));
				}),
				("Was staff during range", () => {
					DateTime? start = PromptDateMaybe("Start date");
					DateTime? end = PromptDateMaybe("End date");
					Console.WriteLine();
					return ("Staff by date", staff.Where(s => (end == null || s.Start < end) && (start == null || s.End == null || s.End > start)));
				}),
				("Was staff for entire range", () => {
					DateTime start = PromptDate("Start date");
					DateTime? end = PromptDate("End date");
					Console.WriteLine();
					return ("Staff by date", staff.Where(s => s.Start <= start && (s.End == null || (end != null && s.End >= end))));
				}),
			},
			IQueryable<Crew> crews => new (string msg, Func<(string name, IQueryable query)> action)[] {
				("Subscribers", () => {
					int? min = PromptIntMaybe("Min");
					int? max = PromptIntMaybe("Max");
					Console.WriteLine();
					return ("Crews by subscribers", crews.Where(c => (min == null || c.Subscribed.Count() >= min) && (max == null || c.Subscribed.Count() <= max)));
				}),
			},
			IQueryable<World> worlds => new (string msg, Func<(string name, IQueryable query)> action)[] {
				("Date created", () => {
					DateTime? start = PromptDateMaybe("Created after");
					DateTime? end = PromptDateMaybe("Created before");
					Console.WriteLine();
					return ("Worlds by date created", worlds.Where(w => (start == null || w.Created >= start) && (end == null || w.Created <= end)));
				}),
				("Play count", () => {
					int? min = PromptIntMaybe("Min");
					int? max = PromptIntMaybe("Max");
					Console.WriteLine();
					return ("Worlds by play count", worlds.Where(w => (min == null || w.Plays >= min) && (max == null || w.Plays <= max)));
				}),
				("Like count", () => {
					int? min = PromptIntMaybe("Min");
					int? max = PromptIntMaybe("Max");
					Console.WriteLine();
					return ("Worlds by likes", worlds.Where(w => (min == null || w.Likes.Count() >= min) && (max == null || w.Likes.Count() <= max)));
				}),
				("Favorite count", () => {
					int? min = PromptIntMaybe("Min");
					int? max = PromptIntMaybe("Max");
					Console.WriteLine();
					return ("Worlds by favorites", worlds.Where(w => (min == null || w.Favorites.Count() >= min) && (max == null || w.Favorites.Count() <= max)));
				}),
			},
			IQueryable<Campaign> campaigns => new (string msg, Func<(string name, IQueryable query)> action)[] {
				("Difficulty", () => {
					CampaignDifficulty? min = PromptDifficultyMaybe("Min");
					CampaignDifficulty? max = PromptDifficultyMaybe("Max");
					return ("Campaigns by difficulty", campaigns.Where(c => (min == null || c.Difficulty >= min) && (max == null || c.Difficulty <= max)));
				}),
			},
			IQueryable<CampaignWorld> campaignWorlds => new (string msg, Func<(string name, IQueryable query)> action)[] {
				("Difficulty", () => {
					CampaignDifficulty? min = PromptDifficultyMaybe("Min");
					CampaignDifficulty? max = PromptDifficultyMaybe("Max");
					return ("Campaign worlds by difficulty", campaignWorlds.Where(c => (min == null || c.Difficulty >= min) && (max == null || c.Difficulty <= max)));
				}),
				("Bronze time", () => {
					uint? min = (PromptUIntMaybe("Min time in ms") + 9) / 10;
					uint? max = PromptUIntMaybe("Max time in ms") / 10;
					Console.WriteLine();
					return ("Campaign worlds by bronze time", campaignWorlds.Where(c => (min == null || c.BronzeTime >= min) && (max == null || c.BronzeTime <= max)));
				}),
				("Silver time", () => {
					uint? min = (PromptUIntMaybe("Min time in ms") + 9) / 10;
					uint? max = PromptUIntMaybe("Max time in ms") / 10;
					Console.WriteLine();
					return ("Campaign worlds by silver time", campaignWorlds.Where(c => (min == null || c.SilverTime >= min) && (max == null || c.SilverTime <= max)));
				}),
				("Gold time", () => {
					uint? min = (PromptUIntMaybe("Min time in ms") + 9) / 10;
					uint? max = PromptUIntMaybe("Max time in ms") / 10;
					Console.WriteLine();
					return ("Campaign worlds by gold time", campaignWorlds.Where(c => (min == null || c.GoldTime >= min) && (max == null || c.GoldTime <= max)));
				}),
				("Platinum time", () => {
					uint? min = (PromptUIntMaybe("Min time in ms") + 9) / 10;
					uint? max = PromptUIntMaybe("Max time in ms") / 10;
					Console.WriteLine();
					return ("Campaign worlds by platinum time", campaignWorlds.Where(c => (min == null || c.PlatinumTime >= min) && (max == null || c.PlatinumTime <= max)));
				}),
				("Staff time", () => {
					uint? min = (PromptUIntMaybe("Min time in ms") + 9) / 10;
					uint? max = PromptUIntMaybe("Max time in ms") / 10;
					Console.WriteLine();
					return ("Campaign worlds by staff time", campaignWorlds.Where(c => (min == null || c.StaffTime >= min) && (max == null || c.StaffTime <= max)));
				}),
			},
			IQueryable<CompletedCampaignWorld> completedCampaignWorlds => new (string msg, Func<(string name, IQueryable query)> action)[] {
				("Time", () => {
					uint? min = (PromptUIntMaybe("Min time in ms") + 9) / 10;
					uint? max = PromptUIntMaybe("Max time in ms") / 10;
					Console.WriteLine();
					return ("Completed campaign worlds by best time", completedCampaignWorlds.Where(c => (min == null || c.Time >= min) && (max == null || c.Time <= max)));
				}),
				("Rank", () => {
					CampaignRank min = PromptCampaignRank("Min rank");
					CampaignRank max = PromptCampaignRank("Max rank");
					ParameterExpression c = Expression.Parameter(typeof(CompletedCampaignWorld), "c");
					return ("Completed campaign worlds by rank", completedCampaignWorlds.Where(
						// c => GetRank(c) >= min && GetRank(c) <= max
						Expression.Lambda<Func<CompletedCampaignWorld, bool>>(
							Expression.And(
								Expression.GreaterThanOrEqual(
									Expression.Invoke(GetRankExpression, c),
									Expression.Constant((sbyte)min)
								),
								Expression.LessThanOrEqual(
									Expression.Invoke(GetRankExpression, c),
									Expression.Constant((sbyte)max)
								)
							),
							c
						)
					));
				}),
			},
		})),
		("Related data", () => FuncMenu("Data", selected switch {
			IQueryable<Player> players => new (string msg, Func<(string name, IQueryable query)> action)[] {
				("Friends", () => ("Friends of player", players.SelectMany(p => p.Friends).Distinct())),
				("Friend graph", () => ("Friend graph of player", players.SelectMany(p => p.FriendsTransitiveClosure).Distinct())),
				("Staff roles", () => ("Staff roles of player", players.SelectMany(p => p.StaffRoles))),
				("Owned crews", () => ("Crews owned by player", players.SelectMany(p => p.OwnedCrews))),
				("Subscribed crews", () => ("Crews subscribed to by player", players.SelectMany(p => p.Subscriptions).Distinct())),
				("Worlds", () => ("Worlds owned by player", players.SelectMany(p => p.Worlds))),
				("Completed campaign worlds", () => ("Campaign worlds completed by player", players.SelectMany(p => p.CompletedCampaignWorlds))),
				("Likes", () => ("Worlds liked by player", players.SelectMany(p => p.Likes).Distinct())),
				("Favorites", () => ("Worlds favorited by player", players.SelectMany(p => p.Favorites).Distinct())),
			},
			IQueryable<Staff> staff => new (string msg, Func<(string name, IQueryable query)> action)[] {
				("Player", () => ("Player corresponding to staff role", staff.Select(s => s.Player).Distinct())),
			},
			IQueryable<Crew> crews => new (string msg, Func<(string name, IQueryable query)> action)[] {
				("Owner", () => ("Crew owner", crews.Select(c => c.Owner).Distinct())),
				("Members", () => ("Crew members", crews.SelectMany(c => c.Members).Distinct())),
				("Subscribed", () => ("Players subscribed to crew", crews.SelectMany(c => c.Subscribed).Distinct())),
				("Worlds", () => ("Worlds owned by crew", crews.SelectMany(c => c.Worlds))),
			},
			IQueryable<World> worlds => new (string msg, Func<(string name, IQueryable query)> action)[] {
				("Owner", () => ("World owner", worlds.Select(w => w.Owner).Distinct())),
				("Crew", () => ("World crew", worlds.Select(w => w.Crew).Distinct())),
				("Likes", () => ("Players that like the world", worlds.SelectMany(w => w.Likes).Distinct())),
				("Favorites", () => ("Players that favorite the world", worlds.SelectMany(w => w.Favorites).Distinct())),
				("World portals", () => ("Worlds through world portals", worlds.SelectMany(w => w.WorldPortals).Distinct())),
				("World portal graph", () => ("Worlds reachable through world portals", worlds.SelectMany(w => w.WorldPortalsTransitiveClosure).Distinct())),
				("Other world portals", () => ("Worlds with portals to this world", worlds.SelectMany(w => w.OtherWorldPortals).Distinct())),
				("Other world portal graph", () => ("Worlds this world is reachable from through world portals", worlds.SelectMany(w => w.OtherWorldPortalsTransitiveClosure).Distinct())),
				("Campaign world", () => ("Campaign world corresponding to world", worlds.Select(w => w.CampaignWorld))),
			},
			IQueryable<Campaign> campaigns => new (string msg, Func<(string name, IQueryable query)> action)[] {
				("Campaign worlds", () => ("Campaign worlds in campaign", campaigns.SelectMany(c => c.CampaignWorlds))),
			},
			IQueryable<CampaignWorld> campaignWorlds => new (string msg, Func<(string name, IQueryable query)> action)[] {
				("Campaign", () => ("Campaign containing campaign world", campaignWorlds.Select(c => c.Campaign).Distinct())),
				("World", () => ("World corresponding to campaign world", campaignWorlds.Select(c => c.World))),
				("Completed", () => ("Completions of campaign world", campaignWorlds.SelectMany(c => c.Completed))),
			},
			IQueryable<CompletedCampaignWorld> completedCampaignWorlds => new (string msg, Func<(string name, IQueryable query)> action)[] {
				("Player", () => ("Player that completed campaign world", completedCampaignWorlds.Select(s => s.Player).Distinct())),
				("Campaign world", () => ("Campaign world that player completed", completedCampaignWorlds.Select(s => s.CampaignWorld).Distinct())),
			},
		})),
	});
	if (parts.Count > 0) options.Add(("Continue earlier query", () => {
		selected = SelectMenu("Query to continue", parts.Select((p, i) => (p.name, p.query)).ToArray());
		return null;
	}));
	if (selected != null)
	{
		options.AddRange(new (string msg, Func<(string name, IQueryable query)?> action)[] {
			("Combine with earlier query", () => {
				(string name, IQueryable query) CombineSame<T>(IQueryable<T> p1, string typeName)
				{
					List<(string msg, Func<(string name, IQueryable query)> action)> options = new();

					for (int j = 0; j < parts.Count; j++)
					{
						if (parts[j].query is IQueryable<T> p2)
						{
							options.Add((parts[j].name, (Func<(string name, IQueryable query)>)(() => FuncMenu("Combination type", new (string msg, Func<(string name, IQueryable query)> action)[] {
								("X and Y", () => ($"{typeName} in both queries", p1.Intersect(p2))),
								("X or Y", () => ($"{typeName} in either query", p1.Union(p2))),
							}))));
						}
					}

					return FuncMenu("Combine with", options);
				}

				return selected switch {
					IQueryable<Player> players => CombineSame(players, "Players"),
					IQueryable<Staff> staff => CombineSame(staff, "Staff"),
					IQueryable<Crew> crews => CombineSame(crews, "Crews"),
					IQueryable<World> worlds => CombineSame(worlds, "Worlds"),
					IQueryable<Campaign> campaigns => CombineSame(campaigns, "Campaigns"),
					IQueryable<CampaignWorld> campaignWorlds => CombineSame(campaignWorlds, "Campaign worlds"),
					IQueryable<CompletedCampaignWorld> completedCampaignWorlds => CombineSame(completedCampaignWorlds, "Completed campaign worlds"),
				};
			}),
			("View result", () => {
				switch (selected)
				{
					case IQueryable<Player> query: {
						List<Player> result = query.ToList();
						for (int i = 0; i < result.Count; i++)
						{
							Player p = result[i];
							Console.WriteLine($"{i + 1}.");
							Console.WriteLine($"\tId: {p.Id}");
							Console.WriteLine($"\tName: {p.Name.ToUpper()}");
							Console.WriteLine($"\tEnergy: {p.Energy}");
							Console.WriteLine($"\tCreated: {p.Created}");
							Console.WriteLine($"\tLastLogin: {p.LastLogin}");
							Console.WriteLine();
						}
					} break;

					case IQueryable<Staff> query: {
						List<Staff> result = db.Staff
							.Where(s => query.Any(s2 => s.RowId == s2.RowId)) // Not sure why this is needed, EFCore has some weird restriction that you can only use includes on the type you started with...
							.Include(w => w.Player)
							.ToList();
						for (int i = 0; i < result.Count; i++)
						{
							Staff s = result[i];
							Console.WriteLine($"{i + 1}.");
							Console.WriteLine($"\tPlayer: {s.Player.Name.ToUpper()}");
							Console.WriteLine($"\tDomain: {s.Domain}");
							Console.WriteLine($"\tRole: {s.Role}");
							Console.WriteLine($"\tStart: {s.Start}");
							Console.WriteLine($"\tEnd: {s.End}");
							Console.WriteLine();
						}
					} break;

					case IQueryable<Crew> query: {
						List<Crew> result = db.Crew
							.Where(c => query.Any(c2 => c.RowId == c2.RowId)) // Not sure why this is needed, EFCore has some weird restriction that you can only use includes on the type you started with...
							.Include(c => c.Owner)
							.ToList();
						for (int i = 0; i < result.Count; i++)
						{
							Crew c = result[i];
							Console.WriteLine($"{i + 1}.");
							Console.WriteLine($"\tId: {c.Id}");
							Console.WriteLine($"\tName: {c.Name}");
							Console.WriteLine($"\tOwner: {c.Owner.Name.ToUpper()}");
							Console.WriteLine();
						}
					} break;

					case IQueryable<World> query: {
						List<World> result = db.World
							.Where(w => query.Any(w2 => w.RowId == w2.RowId)) // Not sure why this is needed, EFCore has some weird restriction that you can only use includes on the type you started with...
							.Include(w => w.Owner)
							.Include(w => w.Crew)
							.ToList();
						for (int i = 0; i < result.Count; i++)
						{
							World w = result[i];
							Console.WriteLine($"{i + 1}.");
							Console.WriteLine($"\tId: {w.Id}");
							Console.WriteLine($"\tName: {w.Name}");
							Console.WriteLine($"\tOwner: {w.Owner.Name.ToUpper()}");
							Console.WriteLine($"\tCrew: {w.Crew?.Name}");
							Console.WriteLine($"\tDescription: {w.Description}");
							Console.WriteLine($"\tPlays: {w.Plays}");
							Console.WriteLine($"\tWidth: {w.Width}");
							Console.WriteLine($"\tHeight: {w.Height}");
							Console.WriteLine($"\tCreated: {w.Created}");
							Console.WriteLine();
						}
					} break;

					case IQueryable<Campaign> query: {
						List<Campaign> result = query
							.ToList();
						for (int i = 0; i < result.Count; i++)
						{
							Campaign c = result[i];
							Console.WriteLine($"{i + 1}.");
							Console.WriteLine($"\tId: {c.Id}");
							Console.WriteLine($"\tName: {c.Name}");
							Console.WriteLine($"\tDescription: {c.Description}");
							Console.WriteLine($"\tDifficulty: {c.Difficulty}");
							Console.WriteLine();
						}
					} break;

					case IQueryable<CampaignWorld> query: {
						List<CampaignWorld> result = db.CampaignWorld
							.Where(c => query.Any(c2 => c.RowId == c2.RowId)) // Not sure why this is needed, EFCore has some weird restriction that you can only use includes on the type you started with...
							.Include(c => c.Campaign)
							.Include(c => c.World)
							.ToList();
						for (int i = 0; i < result.Count; i++)
						{
							CampaignWorld c = result[i];
							Console.WriteLine($"{i + 1}.");
							Console.WriteLine($"\tCampaign: {c.Campaign.Name}");
							Console.WriteLine($"\tStage: {c.Stage + 1}");
							Console.WriteLine($"\tWorld name: {c.World.Name}");
							Console.WriteLine($"\tWorld ID: {c.World.Id}");
							Console.WriteLine($"\tDifficulty: {c.Difficulty}");
							Console.WriteLine($"\tBronze time: {c.BronzeTime}");
							Console.WriteLine($"\tSilver time: {c.SilverTime}");
							Console.WriteLine($"\tGold time: {c.BronzeTime}");
							Console.WriteLine($"\tPlatinum time: {c.PlatinumTime}");
							Console.WriteLine($"\tStaff time: {c.StaffTime}");
							Console.WriteLine();
						}
					} break;

					case IQueryable<CompletedCampaignWorld> query: {
						List<CompletedCampaignWorld> result = db.CompletedCampaignWorld
							.Where(c => query.Any(c2 => c.PlayerId == c2.PlayerId && c.CampaignWorldId == c2.CampaignWorldId)) // Not sure why this is needed, EFCore has some weird restriction that you can only use includes on the type you started with...
							.Include(c => c.Player)
							.Include(c => c.CampaignWorld)
								.ThenInclude(c => c.Campaign)
							.Include(c => c.CampaignWorld)
								.ThenInclude(c => c.World)
							.ToList();
						for (int i = 0; i < result.Count; i++)
						{
							CompletedCampaignWorld c = result[i];
							Console.WriteLine($"{i + 1}.");
							Console.WriteLine($"\tPlayer: {c.Player.Name.ToUpper()}");
							Console.WriteLine($"\tCampaign: {c.CampaignWorld.Campaign.Name}");
							Console.WriteLine($"\tStage: {c.CampaignWorld.Stage + 1}");
							Console.WriteLine($"\tWorld name: {c.CampaignWorld.World.Name}");
							Console.WriteLine($"\tWorld ID: {c.CampaignWorld.World.Id}");
							Console.WriteLine($"\tTime: {c.Time}");
							Console.WriteLine($@"{"\t"}Rank: {GetRank(c)}");
							Console.WriteLine();
						}
					} break;
				}
				return null;
			}),
			("Count resulting entries", () => {
				Console.WriteLine(((IQueryable<object>)selected).Count());
				Console.WriteLine();
				return null;
			}),
		});

		if (selected is IQueryable<World> worldQuery) options.AddRange(new (string msg, Func<(string name, IQueryable query)?> action)[] {
			("Export worlds individually", () => {
				string directory = Prompt("Export directory");
				Directory.CreateDirectory(directory);
				Console.WriteLine("Loading worlds...");
				List<World> result = db.World
					.Where(w => worldQuery.Any(w2 => w.RowId == w2.RowId)) // Not sure why this is needed, EFCore has some weird restriction that you can only use includes on the type you started with...
					.Include(w => w.Owner)
					.Include(w => w.Crew)
					.Include(w => w.CampaignWorld)
					.Include(w => w.Data)
					.ToList();
				Console.WriteLine("Exporting...");
				foreach (World w in result)
				{
					Level level = w.EELVL;
					string filename = MakeFilenameValid($"{(w.Name.Length > 20 ? w.Name.Substring(0, 20) : w.Name)} - {w.Owner.Name} - {w.Id}") + ".eelvl";
					level.Save(Path.Join(directory, filename));
				}
				Console.WriteLine("Done!");
				Console.WriteLine();
				return null;
			}),
			("Export worlds as bundle", () => {
				string archivename = Prompt("Bundle file");
				if (!archivename.EndsWith(".eelvls") && !archivename.EndsWith(".zip")) archivename += ".eelvls";
				Console.WriteLine("Loading worlds...");
				List<World> result = db.World
					.Where(w => worldQuery.Any(w2 => w.RowId == w2.RowId)) // Not sure why this is needed, EFCore has some weird restriction that you can only use includes on the type you started with...
					.Include(w => w.Owner)
					.Include(w => w.Crew)
					.Include(w => w.CampaignWorld)
					.Include(w => w.Data)
					.ToList();
				int startIndex = SelectMenu("Default world", result.Select((w, i) => ($"{w.Name} by {w.Owner.Name.ToUpper()} ({w.Id})", i)).ToArray());
				World first = result[startIndex];
				result.RemoveAt(startIndex);
				result.Insert(0, first);
				bool remap = PromptBool("Because of a limitation of the .eelvls bundle format, world portals will need to be remapped to allow world portals to work.\nRemap world portals?");
				Console.WriteLine("Exporting...");
				LevelBundle bundle = new(result.Select(w => new LevelBundleEntry(
					w.EELVL,
					remap ? w.Id : null,
					MakeFilenameValid($"{(w.Name.Length > 20 ? w.Name.Substring(0, 20) : w.Name)} - {w.Owner.Name} - {w.Id}") + ".eelvl"
				)));
				bundle.Save(archivename);
				Console.WriteLine("Done!");
				Console.WriteLine();
				return null;
			}),
		});
	}

	if (FuncMenu("What next?", options) is (string name, IQueryable query) newQuery)
	{
		parts.Add(newQuery);
		selected = newQuery.query;
	}
}
